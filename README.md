myAVR MK2 example: SMX LED
==========================

example program for Switch Matrix input and LED output

Copyright (c) 2016 Martin Singer <martin.singer@web.de>


Requires
--------

* avr-g++
* avrdude


Configuration
-------------

* mySmartUSB MK2 programmer
* myAVR MK2 board
* Switch matrix board (selfmade)

<http://www.myavr.com>


About
-----

* Switch Matrix (SMX):
  5 columns, 5 rows

* Controller:
  AVR ATmega8L, 3.6864 MHz

* Function:
  - Polls all keys for chainging their state
    (no debounce function)
  - Init driver port as output and set all pins HIGH
  - Init reader port as  input and set all pull-up resistors
  - For polling a key, set the driver row LOW
    and check the reader column for LOW signal.
  - Shows LED signs for the pressed AND the released key.
      Shows first the row, then the column.
      LED signs are binary (0:=green, 1:=yellow, 2:=red)

* Circuit:
  - LED
      + PortD.5 := LED green
      + PortD.6 := LED yellow
      + PortD.7 := LED red

    - SMX (Switch Matrix)
      + PortB.0 := SMX D0 (driver, rows, blue, -)
      + PortB.1 := SMX D1
      + PortB.2 := SMX D2
      + PortB.3 := SMX D3
      + PortB.4 := SMX D4

      + PortC.0 := SMX R0 (reader, cols, white, +)
      + PortC.1 := SMX R1
      + PortC.2 := SMX R2
      + PortC.3 := SMX R3
      + PortC.4 := SMX R4


Schematic: Switch Matrix
------------------------

	+
	Port.C0 -----------------------.
	Port.C1 --------------------.  |
	Port.C2 -----------------.  |  |
	Port.C3 --------------.  |  |  |
	Port.C4 -----------.  |  |  |  |
	                   |  |  |  |  |
	Port.B4 -----------X--X--X--X--X 5
	Port.B3 --------.  |  |  |  |  |
	Port.B2 ------. '--X--X--X--X--X 4
	Port.B1 ----. |    |  |  |  |  |
	Port.B0 --. | '----X--X--X--X--X 3
	-         | |      |  |  |  |  |   r
	          | '------X--X--X--X--X 2 o
	          |        |  |  |  |  |   w
	          '--------X--X--X--X--X 1
	                   5  4  3  2  1
	                          c o l

